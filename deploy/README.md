Before running the application, below are the requirements:
1. Install Java 8 
2. Install NodeJs
3. Then run "npm install express -g" //IMPORTANT
4. Run "npm install http-server -g"  //IMPORTANT
5. Install Angular "sudo npm intall -g @angular/cli" //IMPORTANT

Instructions:
Note: we need two terminals to run our application, one for running the server and other for running client server
1. First start the java application - to start the applcation run "java -jar technologies-0.0.1-SNAPSHOT.jar" in one terminal
2. Now open another terminal and go into "client" folder (cd client)
3. Run the command "npm install"
4. Then run the command "ng build --prod" (and press enter if it reaches 94%)
5. Now run the command "http-server dist/  --proxy http://localhost:9090 -p 4200" (If you are using windows then run "http-server .\dist\  --proxy http://localhost:9090 -p 4200")
	Note: If you get and "Express is not found error" then run "npm install express -g"

6. Now you open the application in http://localhost:4200
	
	
	
#######################################################################################
Run the below commands only if you are unable to see the Angular app running.
 
Even after running the STEP:5 which is mentioned above in the Instructions section, you might not be able to see the app running.
To solve this kind of problem, 
1. stop the http-server (cmd+c/ctrl+c)
2. Now run ng serve --proxy-config proxy.conf.json. This will run the inbuilt Webpack.js server.
#######################################################################################

